
public class Fibonacci {
	public static void main(String[] args) {
		//get the number of numbers to print
		System.out.println("Fibonacci sequence output: \n");
		
		//init and print the first two numbers
		int M,N,A;
		int i=17, j=0;
		M = 1;
		N = 2;
		while (j!=i) {
			fibonacci(M);
			A = M+N;
			M = N;
			N = A;
			j++;
		}
	}
	//print the output
	public static void fibonacci(int output) {
			System.out.print(output + " ");
		
		}
	}
	//Cabangunay, Csean Karll
	//Betonio, Kyle Joshua
